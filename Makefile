all:

32: ubuntu-22.04-32
64: ubuntu-22.04-64

ubuntu-22.04-32:
	docker build -t ubuntu-22.04-32 -f ubuntu-22.04/Dockerfile.32 ubuntu-22.04

ubuntu-22.04-64:
	docker build -t ubuntu-22.04-64 -f ubuntu-22.04/Dockerfile.64 ubuntu-22.04
