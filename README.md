# Prepare docker build containers

For 32 bit builds:

    make 32

For 64 bit builds:

    make 64

# Run build container for 32 bit builds

On the host:

    cd $WINE_SOURCE

    mkdir build.32

    docker run -it --rm -v $PWD:/home/ubuntu/wine ubuntu-22.04-32

In the container:

    cd ~/wine/build.32

    ../configure CC="ccache gcc" CROSSCC="ccache i686-w64-mingw32-gcc"

# Run build container for 64 bit builds

On the host:

    cd $WINE_SOURCE

    mkdir build.64

    docker run -it --rm -v $PWD:/home/ubuntu/wine ubuntu-22.04-64

In the container:

    cd ~/wine/build.64

    ../configure CC="ccache gcc" CROSSCC="ccache x86_64-w64-mingw32-gcc"

